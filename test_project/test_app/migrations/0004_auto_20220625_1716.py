from django.db import migrations, models


from django.db import migrations
import uuid


def gen_uuid(apps, schema_editor):
    TestModel = apps.get_model('test_app', 'TestModel')
    for row in TestModel.objects.all():
        row.uuid = uuid.uuid4()
        row.save(update_fields=['code'])


class Migration(migrations.Migration):

    dependencies = [
        ('test_app', '0003_auto_20220625_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testmodel',
            name='code',
            field=models.UUIDField(default=uuid.uuid4, unique=True, null=False),
        ),
    ]