from django.db import models
import uuid


class TestModel(models.Model):
    name = models.TextField(unique=False, null=False, max_length=50)
    surname = models.TextField(unique=False, null=True, max_length=50)
    code = models.UUIDField(default=uuid.uuid4, unique=True)
    is_close = models.BooleanField(default=False)
