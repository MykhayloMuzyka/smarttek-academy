from django.core.management.base import BaseCommand, CommandError
from test_app.models import TestModel
from django.core.exceptions import ObjectDoesNotExist


class Command(BaseCommand):
    help = 'Closes the account'

    def add_arguments(self, parser):
        parser.add_argument('user_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        for user_id in options['user_ids']:
            try:
                user = TestModel.objects.get(pk=user_id)
            except ObjectDoesNotExist:
                raise CommandError('User "%s" does not exist' % user_id)

            user.is_active = False
            user.save(update_fields=['is_active'])

            self.stdout.write(self.style.SUCCESS('Successfully closed account of user "%s"' % user_id))
